##Práctica| Mi primera aplicación con NPM
###### **Primero creamos un directorio llamado "package_manager"**
En mi caso, me posicione en C: y un ahi cree una carpeta llamada "web" y dentro de esta hize el directorio "package_manager".

![](https://raw.githubusercontent.com/miguel-cortinas/flappy/main/creacion%20package.png?token=GHSAT0AAAAAAB6QVBCW2QS6POKKHWUVS24KY7EGFRQ)

###### **Generamos los siguientes archivos:**

![](https://raw.githubusercontent.com/miguel-cortinas/flappy/main/doc%20package.png?token=GHSAT0AAAAAAB6QVBCWKVDICM4H4XZOI5TQY7EGQWA)

######  **En Git Bash:**

![](https://raw.githubusercontent.com/miguel-cortinas/flappy/main/en%20gitbach.png?token=GHSAT0AAAAAAB6QVBCXHTFKMYJZWZ7I3LTKY7EG2JQ)

###### **En github:**

![](https://raw.githubusercontent.com/miguel-cortinas/flappy/main/github.png?token=GHSAT0AAAAAAB6QVBCXUIZNIHIKNFD257ACY7EG5MA)

###### **En gitlab:**

![](https://raw.githubusercontent.com/miguel-cortinas/flappy/main/gitlab.png?token=GHSAT0AAAAAAB6QVBCXW5WT4ZPPAWMGSGM6Y7EG7XA)